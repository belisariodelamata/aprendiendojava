
public class PrincipalMatriz {

    public static void main(String[] args) {
        //int[][] matriz=new int[5][3];
        int[][] matriz=new int[][]{
            {80,30,20},
            {30,25,22},
            {30,14,2,2,326,5,6,56}
        };
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j]+",");
            }
            System.out.println("");
        }
        System.out.println("------");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print("("+i+","+j+") ");
            }
            System.out.println("");
        }
        
        int tablero[][]=new int[10][10];
        
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                if (j%2==0){
                    System.out.print("█");//Alt+987 TABLA ASCII
                }else{
                    System.out.print("*");
                }
            }
            System.out.println("");
        }
    }
}
