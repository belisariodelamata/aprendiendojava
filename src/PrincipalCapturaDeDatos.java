
import java.util.Scanner;

public class PrincipalCapturaDeDatos {

    static Scanner scanner;
    
    public static void main(String[] args) {

        scanner = new Scanner(System.in);

        System.out.println("BIENVENIDO");
        System.out.println(scanner.next());
//        System.out.println("Por favor, digite su nombre:");
//        String nombre=scanner.nextLine();
//        System.out.println("Hola, mucho gusto "+nombre);
//        System.out.println("Por favor digite su edad:");
//        int edad= scanner.nextInt();
//        System.out.println("Hola, ya sé que tienes "+edad+ " años.");
//        
//        if (edad>=18){
//            System.out.println("Usted es mayor de edad.");
//        }else{
//            System.out.println("Usted no es mayor de edad");
//        }
//        
//        String mensajeMostrar=(edad>=18)?"Usted es mayor de edad.":"Usted no es mayor de edad";
//        System.out.println(mensajeMostrar);
//        System.out.println("----");
//        System.out.println((edad>=18)?"Usted es mayor de edad.":"Usted no es mayor de edad");
        System.out.println("MENU DE OPCIONES");
        System.out.println("1. SUMAR VALORES");
        System.out.println("2. RESTAR VALORES");
        System.out.println("3. MULTIPLICAR VALORES");
        System.out.println("Por favor digite una opción:");
        int opcion = scanner.nextInt();

//        if (opcion == 1) {
//            System.out.println("Se va a ejecutar el programa de suma");
//        } else if (opcion == 2) {
//            System.out.println("Se va a ejecutar el programa de resta");
//        } else if (opcion == 3) {
//            System.out.println("Se va a ejecutar el programa de multiplicación");
//        } else {
//            System.out.println("Opción inválida");
//        }

        switch (opcion) {
            case 1:
                System.out.println("Se va a ejecutar el programa de suma");
                programaSuma();
                break;
            case 2:
                System.out.println("Se va a ejecutar el programa de resta");
                programaResta();
                break;
            case 3:
                System.out.println("Se va a ejecutar el programa de multiplicación");
                break;
            default:
                System.out.println("Opción inválida");
        }
        
        System.out.println("Hola, se terminó el programa");
    }
    
    static void programaSuma(){
        System.out.println("Digite el sumando 1:");
        int numero1=scanner.nextInt();
        System.out.println("Digite el sumando 2:");
        int numero2=scanner.nextInt();
        System.out.println("El resultado es "+(numero1+numero2));
    }   
    static void programaResta(){
        System.out.println("Digite el número 1:");
        int numero1=scanner.nextInt();
        System.out.println("Digite el número 2:");
        int numero2=scanner.nextInt();
        System.out.println("El resultado es "+(numero1-numero2));
        
//        sumar(numero1, numero2);
//        sumar(numero1, numero2, numero2, numero1);
//        sumar(numero1, numero2, numero2, numero2);
//        sumar(new int[]{numero1, numero2});
    }
    
//    static int sumar(int[] valores){
//        return 0;
//    }
//    static int sumar(int... valores){
//        return 0;
//    }
    
    
}
