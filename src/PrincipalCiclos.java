
/**
 *
 * @author BELSOFT
 */
public class PrincipalCiclos {

    public static void main(String[] args) {

        for (int i = 1; i <= 100; i++) {
            //System.out.println(i);
            System.out.print(i+",");
        }
        
        ///////
        System.out.println("-------------");
        int i = 1;
        while (i <=100) {
            //System.out.println(i);
            System.out.print(i+",");
            i++;
        }
        //////
        System.out.println("-------------");
        i = 1;
        do {
//            System.out.println(i);
            System.out.print(i+",");
            i++;
        } while (i <=100);
    }

}
