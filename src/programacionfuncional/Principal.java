package programacionfuncional;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import poo.Persona;

/**
 *
 * @author BELSOFT
 */
public class Principal {

    public static List<Persona> iniciarDatosPersona(){
        List<Persona> personas=new LinkedList<>();
        personas.add(new Persona("BELISARIO", "DE LA MATA", 25));
        personas.add(new Persona("CAROLINA", "RAMIREZ", 30));
        personas.add(new Persona("LIZZETH", "SANCHEZ", 20));
        personas.add(new Persona("YERALDIN", "PALOMINA", 18));
        personas.add(new Persona("ELMER", "MEKEL", 25));
        personas.add(new Persona("MIGUEL", "SANZ", 31));
        personas.add(new Persona("OSVALDO", "RIOS", 35));
        personas.add(new Persona("JANDER", "ARGUELLO", 28));
        personas.add(new Persona("ELIANA", "CASTRO", 21));
        personas.add(new Persona("NATALINA", "MOLINA", 26));
        return personas;
    }
    
    public static void main(String[] args) {
        ///PROGRAMACIÓN ABSTRACTA A TRAVÉS DE INTERFACES
        List<Persona> personas=iniciarDatosPersona();        
        for(Persona p: personas){
            System.out.println(p.getPrimerNombre()+","+p.getEdad());
        }
        System.out.println("--------");
        personas.forEach(new Consumer<Persona>(){
            @Override
            public void accept(Persona p) {
                System.out.println(p.getPrimerNombre()+","+p.getEdad());
            }
        });
        System.out.println("----UTILIZANDO PROGRAMACION FUNCIONAL - LAMBDA");
        personas.forEach(persona->System.out.println(persona.getPrimerNombre()+","+persona.getEdad()));
        
    }   
    
}
