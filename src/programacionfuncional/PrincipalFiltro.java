package programacionfuncional;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import poo.Persona;

/**
 *
 * @author BELSOFT
 */
public class PrincipalFiltro {

    public static List<Persona> iniciarDatosPersona(){
        List<Persona> personas=new LinkedList<>();
        personas.add(new Persona("BELISARIO", "DE LA MATA", 25));
        personas.add(new Persona("CAROLINA", "RAMIREZ", 30));
        personas.add(new Persona("LIZZETH", "SANCHEZ", 20));
        personas.add(new Persona("YERALDIN", "PALOMINA", 18));
        personas.add(new Persona("ELMER", "MEKEL", 25));
        personas.add(new Persona("MIGUEL", "SANZ", 31));
        personas.add(new Persona("OSVALDO", "RIOS", 35));
        personas.add(new Persona("JANDER", "ARGUELLO", 28));
        personas.add(new Persona("ELIANA", "CASTRO", 21));
        personas.add(new Persona("NATALINA", "MOLINA", 26));
        return personas;
    }
    
    public static void main(String[] args) {
        ///PROGRAMACIÓN ABSTRACTA A TRAVÉS DE INTERFACES
        List<Persona> personas=iniciarDatosPersona();        
        for(Persona p: personas){
            System.out.println(p.getPrimerNombre()+","+p.getEdad());
        }
        
        
        System.out.println("Solución Clásica - Tradicional");
        for(Persona p: personas){
            if (p.getEdad()>25){
                System.out.println(p.getPrimerNombre()+","+p.getEdad());
            }
        }
        System.out.println("Solución Con Implementación de la Interfaz");
        personas.stream().filter(new Predicate<Persona>(){
            @Override
            public boolean test(Persona p1) {
                return p1.getEdad()>25;
            }
        }).forEach(new Consumer<Persona>() {
            @Override
            public void accept(Persona p1) {
                System.out.println(p1.getPrimerNombre()+","+p1.getEdad());
            }
        });
        System.out.println("----UTILIZANDO PROGRAMACION FUNCIONAL - LAMBDA");
        
        personas.stream().filter(p1->p1.getEdad()>25).forEach(p1->System.out.println(p1.getPrimerNombre()+","+p1.getEdad()));
        
    }   
    
}
