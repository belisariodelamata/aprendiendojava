/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacionfuncional;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author BELSOFT
 */
public class PrincipalOrdenamientoPrimitivos {
    public static void main(String[] args) {
        Integer[] valor={10,15,100,200,5,12};
        
        List<Integer> valoresLista=Arrays.asList(valor);
        
        Collections.sort(valoresLista);
        
        valoresLista.forEach(System.out::println);
        
        valoresLista.forEach(new Consumer<Integer>(){
            @Override
            public void accept(Integer t) {
                System.out.println(t);
            }
        });
    }
}
