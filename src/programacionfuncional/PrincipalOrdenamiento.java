package programacionfuncional;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import poo.Persona;

/**
 *
 * @author BELSOFT
 */
public class PrincipalOrdenamiento {

    public static List<Persona> iniciarDatosPersona(){
        List<Persona> personas=new LinkedList<>();
        personas.add(new Persona("BELISARIO", "DE LA MATA", 25));
        personas.add(new Persona("DIANA", "RAMIREZ", 30));
        personas.add(new Persona("CAROLINA", "RAMIREZ", 30));
        personas.add(new Persona("LIZZETH", "SANCHEZ", 20));
        personas.add(new Persona("YERALDIN", "PALOMINA", 18));
        personas.add(new Persona("ELMER", "MEKEL", 25));
        personas.add(new Persona("MIGUEL", "SANZ", 31));
        personas.add(new Persona("OSVALDO", "RIOS", 35));
        personas.add(new Persona("JANDER", "ARGUELLO", 28));
        personas.add(new Persona("ELIANA", "CASTRO", 21));
        personas.add(new Persona("NATALINA", "MOLINA", 26));
        return personas;
    }
    
    public static void main(String[] args) {
        ///PROGRAMACIÓN ABSTRACTA A TRAVÉS DE INTERFACES
        List<Persona> personas=iniciarDatosPersona();        
        for(Persona p: personas){
            System.out.println(p.getPrimerNombre()+","+p.getEdad());
        }
        
        System.out.println("Solución Con Implementación de la Interfaz");
        Collections.sort(personas, new Comparator<Persona>(){
            @Override
            public int compare(Persona p1, Persona p2) {
                //Valor 1 Comparado con Valor 2
                //Si son Iguales el Retorno es 0
                //Si el Valor 1 es menor que el valor 2 el retorno debe ser menor que 0
                //Si el Valor 2 es menor que el valor 1 el retorno debe ser mayor que 0
                return p1.getPrimerNombre().compareTo(p2.getPrimerNombre());
//                if (p1.getEdad()==p2.getEdad()){
//                    return 0;
//                }else if (p1.getEdad()<p2.getEdad()){
//                    return -1;
//                }else{
//                    return 1;
//                }
            }
            
        });
        System.out.println("Personas Ordenadas");
        for(Persona p: personas){
            System.out.println(p.getPrimerNombre()+","+p.getEdad());
        }
        
        System.out.println("----UTILIZANDO PROGRAMACION FUNCIONAL - LAMBDA");
        Collections.sort(personas, 
                (p1, p2)->
                (p1.getPrimerApellido()+" "+p1.getPrimerNombre())
                .compareTo(p2.getPrimerApellido()+" "+p2.getPrimerNombre())
        );
//        Collections.sort(personas, (p1, p2)->new Integer(p1.getEdad()).compareTo(new Integer(p2.getEdad()))*-1);
        for(Persona p: personas){
            System.out.println(p.getPrimerApellido()+" "+p.getPrimerNombre()+","+p.getEdad());
        }
        
    }   
    
}
