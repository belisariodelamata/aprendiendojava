package poo;

/**
 *
 * @author BELSOFT
 */
public class Estudiante extends Persona implements IActividadesBasicas{

    public void hacerTarea(){
//        primerNombre="BS";
        setPrimerNombre("BS");
    }

    @Override
    public void levantarse() {
        System.out.println("El estudiante se ha levantado");
    }

    @Override
    public void dormise() {
        System.out.println("El estudiante se ha dormido");
    }
    
}
