package poo;

/**
 *
 * @author BELSOFT
 */
public class Docente extends Persona implements IActividadesBasicas{
    
    public void subirActividad(){
        
    }

    @Override
    public void levantarse() {
        System.out.println("El docente se ha levantado");
    }

    @Override
    public void dormise() {
        System.out.println("El docente se ha dormido");
    }
}
