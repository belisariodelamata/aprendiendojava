package poo;

/**
 * @author BELSOFT
 */
public class Principal {
    public static void main(String[] args) {
        Persona persona=new Persona();
        persona.setPrimerNombre("BS");
        persona.setSegundoNombre("JR");

        Estudiante estudiante=new Estudiante();
//        estudiante.primerNombre="XXX";
//        Carro carro=new Carro();
//        carro.marca="BMW";

        Persona docente=new Docente();
        Persona estudiante2=new Estudiante();
        
        Docente docente2=new Docente();
        docente2.subirActividad();
        
        
        IActividadesBasicas docenteI=new Docente();
        IActividadesBasicas estudianteI=new Estudiante();
    }
}
