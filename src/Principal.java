/**
 * @author BELSOFT
 */
public class Principal {
    public static void main(String[] args) {
        System.out.println("Hola mundo");
        
        mostrarMensaje();
        mostrarMensaje("BS");
        
        int valor=2000000;
        
        char caracter='a';
        System.out.println("El Caracter a convertido a numero es "+((float)caracter));
        
        System.out.println(valor);
        System.out.println(caracter);
        
        System.out.println(0.1+0.1+0.1);
        
        //BigDecimal
    }
    
    public static void mostrarMensaje(){
        System.out.println("Este es nuestro segundo mensaje en Java");
    }
    
    public static void mostrarMensaje(String nombre){
        System.out.println("Hola "+nombre);
    }
}
