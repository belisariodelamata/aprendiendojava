
public class PrincipalVectorReversa {
    public static void main(String[] args) {
        int[] valores={100,200,300,400,5,10};
        for (int i = 0; i < valores.length; i++) {
            int valor = valores[i];
            System.out.print(valor+",");
        }
        System.out.println("--");
        System.out.println("---NUMEROS PARES---");
        for (int i = 0; i < valores.length; i++) {
            int valor = valores[i];
            if (valor%2==0){
                System.out.print(valor+",");
            }
        }
        System.out.println("----------");
        for (int i = valores.length-1; i >= 0; i--) {
            int valor = valores[i];
            System.out.print(valor+",");
        }
    }
}
