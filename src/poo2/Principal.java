package poo2;

import poo.Estudiante;

/**
 * @author BELSOFT
 */
public class Principal {
    public static void main(String[] args) {
        Estudiante estudiante=new Estudiante();
        //public , Alcance desde cualquier objeto del sistema
        //private, Alcance desde el mismo objeto
        //protected, Alcance desde sus hijos y el paquete del objeto
        //default (No se escribe), Alcance desde el paquete del objeto
    }
}
