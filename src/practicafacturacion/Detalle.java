package practicafacturacion;

import java.io.Serializable;

/**
 *
 * @author BELSOFT
 */
public class Detalle implements Serializable{

    private int cantidad;
    private Producto producto;
    private float precio;
    private float total;

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getTotal() {
        return total;
    }

//    public void setTotal(float total) {
//        this.total = total;
//    }
    
    public void calcularTotal(){
        this.total=this.getCantidad()*this.getPrecio();
    }
    
}
