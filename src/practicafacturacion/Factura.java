package practicafacturacion;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BELSOFT
 */
public class Factura implements Serializable{
    private long id;
    private Date fechaCreacion;
    private Cliente cliente;

    private List<Detalle> listaDetalle=new LinkedList<>();
    
    private float total;

    public void addDetalle(Detalle detalle){
        detalle.calcularTotal();
        detalle.getProducto().vender(detalle.getCantidad());
//        total=total+detalle.getTotal();
        total+=detalle.getTotal();
        this.getListaDetalle().add(detalle);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Detalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setListaDetalle(List<Detalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }   
    
}
