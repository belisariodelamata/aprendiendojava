package practicafacturacion;

import java.io.Serializable;

/**
 *
 * @author BELSOFT
 */
public class Cliente implements Serializable{
    int id;
    String documentoIdentidad;
    String nombres;
    String apellidos;

    public Cliente() {
        
    }
    public Cliente(String documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }

    @Override
    public boolean equals(Object obj) {
        Cliente clienteComparacion=(Cliente) obj;
        return this.documentoIdentidad.equals(clienteComparacion.documentoIdentidad);
    }
    
    
    
}
