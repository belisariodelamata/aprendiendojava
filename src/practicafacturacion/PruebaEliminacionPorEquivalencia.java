package practicafacturacion;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BELSOFT
 */
public class PruebaEliminacionPorEquivalencia {
    public static void main(String[] args) {
        LinkedList<Cliente> clientesOtraForma=new LinkedList<>();
        
        List<Cliente> clientes=new LinkedList<>();
        LinkedList<Cliente> clientesOtros=new LinkedList<>();
        
        Cliente cliente=new Cliente();
        cliente.documentoIdentidad="XXX1";
        clientes.add(cliente);
        
        cliente=new Cliente();
        cliente.documentoIdentidad="XXX2";
        clientes.add(cliente);

        cliente=new Cliente();
        cliente.documentoIdentidad="XXX3";
        clientes.add(cliente);

        cliente=new Cliente();
        cliente.documentoIdentidad="XXX4";
        clientes.add(cliente);
        
        mostrarClientes(clientes);
        
        boolean eliminado=clientes.remove(new Cliente("XXX3"));
        System.out.println(eliminado?"Elemento Eliminado": "Elemento No Encontrado");
        System.out.println("------");
        mostrarClientes(clientes);
        
    }
    
    private static void mostrarClientes(List<Cliente> clientes){
        for(Cliente cliente: clientes){
            System.out.println(cliente.documentoIdentidad);
        }
    }
}
