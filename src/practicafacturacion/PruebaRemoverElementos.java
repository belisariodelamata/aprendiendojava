/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicafacturacion;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BELSOFT
 */
public class PruebaRemoverElementos {

    public static void main(String[] args) {
        List<Integer> lista=new LinkedList<>();
        lista.add(10);
        lista.add(15);
        lista.add(20);
        lista.add(30);
        lista.add(100);
        mostrarElementos(lista);
        System.out.println("---");
        lista.remove((Integer)100);
//        lista.remove(1);
        mostrarElementos(lista);
        
    }
    
    private static void mostrarElementos(List<Integer> lista){
        for(Integer valor: lista){
            System.out.println(valor);
        }
    }
}
