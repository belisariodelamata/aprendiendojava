package practicafacturacion;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BELSOFT
 */
public class General implements Serializable{

    ////Acceso a las Variables de Entorno - Compartidas
    public static General general=null;

    static{
        iniciarDatos();
    }
    
    public static void iniciarDatos(){
        General tmpGeneral=(General)SerializarObjeto.deserializarObjeto("EstadoProductos.dat");
        if (tmpGeneral==null){
            System.out.println("Creando los Datos Iniciales");
            General.general=new General();
        }else{
            System.out.println("Cargando Datos del Archivo");
            General.general=tmpGeneral;
        }        
    }
    
    ////////////////////////////////////////////
    private List<Factura> facturas = new LinkedList<>();
    private List<Producto> productos = new LinkedList<>();

    public General() {
        iniciarListadoProductos();
    }
    
    private void iniciarListadoProductos() {
        
        Producto producto=new Producto(1, "PRODUCTXXX1", "ALMANAQUE", 5000); 
        producto.setStock(5);
        getProductos().add(producto);
        
        Producto producto2=new Producto(2, "PRODUCTXXX2", "TABLA", 10000);
        producto2.setStock(4);
        getProductos().add(producto2);
        
        producto=new Producto(3, "PRODUCTXXX3", "CINTA", 2000);
        producto.setStock(3);
        getProductos().add(producto);
    }
    
    public void guardar(){
        SerializarObjeto.serializarObjeto("EstadoProductos.dat", this);
    }

    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    
}
