
import java.text.DecimalFormat;

/**
 *
 * @author BELSOFT
 */
public class PruebaEstudiante {
    public static void main(String[] args) {
        Estudiante estudiante=new Estudiante();
        estudiante.primerNombre="Josue";
        estudiante.segundoNombre="Daniel";
        estudiante.primerApellido="Torres";
        estudiante.segundoApellido="Santos";
        
        System.out.println("El primer nombre del estudiante es "+estudiante.primerNombre);
        
        System.out.println("------------------");
        
        Estudiante estudianteDos=new Estudiante();
        estudianteDos.primerNombre="Harold";
        estudianteDos.segundoNombre="Jose";
        estudianteDos.primerApellido="Delgado";
        estudianteDos.segundoApellido="Tejeda";
        
        ///////////////
        Estudiante[] estudiantes=new Estudiante[2];
        estudiantes[0]=new Estudiante();
        estudiantes[0].primerNombre="XXXX";
        estudiantes[0].primerApellido="YYYY";
        
        estudiantes[1]=new Estudiante();
        estudiantes[1].primerNombre="AAA";
        estudiantes[1].primerApellido="BBB";
        ////////////////
        mostrarEstudiantes(estudiantes);
        
        System.out.println(new DecimalFormat("00000000").format(11));
    }
    
    public static void mostrarEstudiantes(Estudiante[] estudiantes){
        System.out.println("El estudiante en posicion 0 es "+estudiantes[0].primerNombre);
        System.out.println("El estudiante en posicion 1 es "+estudiantes[1].primerNombre);
    }
}
