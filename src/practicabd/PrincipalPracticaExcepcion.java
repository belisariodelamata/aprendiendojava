/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicabd;

/**
 *
 * @author BELSOFT
 */
public class PrincipalPracticaExcepcion {

    public static void main(String[] args) {
        try {
            int num = Integer.parseInt("100XXX");
            System.out.println(num);
        } catch (NumberFormatException e) {
            System.out.println("Se ha generado un error "+e.getMessage());
        }
    }
}
