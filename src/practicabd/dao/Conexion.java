package practicabd.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author BELSOFT
 */
public class Conexion {

    static Connection conexion;

    public static Connection getConexion() {
        //Si la conexion no ha sido instanciada
        if (conexion == null) {
            //Crear la Conexion
            String hostname = "localhost";
            String port = "3306";
            String database = "practica_java";
            String url = "jdbc:mariadb://" + hostname + ":" + port + "/" + database + "?useSSL=false";
            String user = "root";
            String password = "";
            try {
                conexion = DriverManager.getConnection(url, user, password);
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return conexion;
    }

    public static void cerrarPsRs(PreparedStatement ps, ResultSet rs) {
        cerrarRs(rs);
        cerrarPs(ps);
    }

    public static void autoCommit(boolean autocommit) throws SQLException {
        getConexion().setAutoCommit(autocommit);
    }

    public static void commit() throws SQLException {
        getConexion().commit();
    }

    public static void rollback() {
        try {
            getConexion().rollback();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void cerrarPs(PreparedStatement ps) {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void cerrarRs(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void cerrar() {
        try {
            getConexion().close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        System.out.println("Inicio de Conexion");
        getConexion();
        System.out.println("Conexión Exitosa");
    }
}
