package practicabd.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import practicabd.domain.Producto;

/**
 *
 * @author BELSOFT
 */
public class ProductoDAO {

    public List<Producto> listar() {
        List<Producto> lista = new LinkedList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            //Obtenemos la Conexion y Preparamos la Consulta
            ps = Conexion.getConexion().prepareStatement("select * from producto");
            //Ejecutamos la Consulta
            rs = ps.executeQuery();
            //Verificacion si hay registros
            while (rs.next()) {
                //Creamos un Objeto por Cada registro de la consulta
                Producto producto = new Producto();
                producto.setId(rs.getInt("id"));
                producto.setCodigo(rs.getString("codigo"));
                producto.setNombre(rs.getString("nombre"));
                //Agregamos el Objeto a la Lista Resultante
                lista.add(producto);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Conexion.cerrarPsRs(ps, rs);
        }

        return lista;
    }

    /**
     * C Create
     *
     * @param producto
     * @return
     */
    public boolean insertar(Producto producto) {
        boolean sw = false;

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = Conexion.getConexion().prepareStatement(
                    "insert into producto (codigo, nombre) values(?, ?)",
                    new String[]{"id"});
            ps.setString(1, producto.getCodigo());
            ps.setString(2, producto.getNombre());
            if (ps.executeUpdate() > 0) {
                sw = true;
                rs = ps.getGeneratedKeys();
                //Movimiento en el resultado de una consulta
                rs.next();
                producto.setId(rs.getInt("id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Conexion.cerrarPsRs(ps, rs);
        }
        return sw;
    }

    public boolean eliminarPorId(int id) {
        boolean sw = false;
        PreparedStatement ps = null;
        try {
            ps = Conexion.getConexion().prepareStatement("delete from producto where id=?");
            ps.setInt(1, id);
//            if (ps.executeUpdate() > 0) {
//                sw = true;
//            }
            sw = ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(ProductoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Conexion.cerrarPs(ps);
        }
        return sw;
    }

    public boolean eliminarPorNombre(String nombre) {
        boolean sw = false;
        PreparedStatement ps = null;
        try {
            ps = Conexion.getConexion().prepareStatement("delete from producto where nombre=?");
            ps.setString(1, nombre);
            sw = ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(ProductoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Conexion.cerrarPs(ps);
        }
        return sw;
    }

    public boolean actualizar(Producto producto) {
        boolean sw = false;
        PreparedStatement ps = null;
        try {
            ps = Conexion.getConexion().prepareStatement(
                    "update producto set codigo=?, nombre=? where id=?");
            ps.setString(1, producto.getCodigo());
            ps.setString(2, producto.getNombre());
            ps.setInt(3, producto.getId());
            sw = ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(ProductoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Conexion.cerrarPs(ps);
        }
        return sw;
    }

    public boolean insertar(List<Producto> productos) {
        boolean sw = true;
        ////////////////
        try {
            Conexion.autoCommit(false);
            for (Producto producto : productos) {
                if (!insertar(producto)) {
                    sw = false;
                    Conexion.rollback();
                    throw new SQLException("Error en Transacción");
                }
            }
            Conexion.commit();
        } catch (SQLException ex) {
            Logger.getLogger(ProductoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        ///////////////
        try {
            Conexion.autoCommit(true);
        } catch (SQLException ex) {
            Logger.getLogger(ProductoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sw;
    }

    public boolean insertarOptimizado(List<Producto> productos) {
        boolean sw = false;
        PreparedStatement ps = null;
        try {
            Conexion.autoCommit(false);
            ps = Conexion.getConexion().prepareStatement(
                    "insert into producto (codigo, nombre) values(?, ?)");
            for (Producto producto : productos) {
                ps.clearParameters();
                ps.setString(1, producto.getCodigo());
                ps.setString(2, producto.getNombre());
                ps.executeUpdate();
            }
            Conexion.commit();
            sw = true;
        } catch (SQLException ex) {
            Conexion.rollback();
            Logger.getLogger(ProductoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            Conexion.cerrarPs(ps);
        }
        return sw;
    }

}
