/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicabd;

import java.util.LinkedList;
import java.util.List;
import practicabd.dao.Conexion;
import practicabd.dao.ProductoDAO;
import practicabd.domain.Producto;

/**
 *
 * @author BELSOFT
 */
public class PrincipalPruebaAutoCommit {

    public static void main(String[] args) {
        ProductoDAO productoDAO = new ProductoDAO();
        List<Producto> lista = new LinkedList();

        Producto producto = new Producto();
        producto.setCodigo("XXXX200100");
        producto.setNombre("Nombre100100");
        lista.add(producto);

        producto = new Producto();
        producto.setCodigo("XXXX200110");
        producto.setNombre("Nombre100110");
        lista.add(producto);

        producto = new Producto();
        producto.setCodigo("XXXX200110XXXXXXXXXXXXXXXXXX");
        producto.setNombre("Nombre100110XXXXXXXXXXXXXXXX");
        lista.add(producto);

        productoDAO.insertar(lista);

        Conexion.cerrar();

    }
}
