/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicabd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BELSOFT
 */
public class Principal {

    public static void main(String[] args) throws ClassNotFoundException {
        String hostname = "localhost";
        String port = "3306";
        String database = "ws_practica";
        String url = "jdbc:mariadb://" + hostname + ":" + port + "/" + database + "?useSSL=false";

        //jdbc:postgres://localhost:port/database
        //jdbc:mariadb://127.0.0.1:port/database
        //jdbc:mariadb://localhost:port/database
        //jdbc:mariadb://10.10.2.7:3306/database
        
        String user = "root";
        String password = "";
        /////////////
        Connection connection = null;
        try {
            //Class.forName("org.mariadb.jdbc.Driver");
            connection = DriverManager.getConnection(url, user, password);
            System.out.println("Conexión Ok");
        } catch (SQLException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (connection!=null){
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
    }
}
