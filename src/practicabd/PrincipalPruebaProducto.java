/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicabd;

import java.util.List;
import practicabd.dao.ProductoDAO;
import practicabd.domain.Producto;

/**
 *
 * @author BELSOFT
 */
public class PrincipalPruebaProducto {

    public static void main(String[] args) {
        ProductoDAO productoDAO = new ProductoDAO();
        List<Producto> productos = productoDAO.listar();
        for (Producto producto : productos) {
            System.out.println(producto);
        }

//        boolean sw = productoDAO.eliminarPorId(9);
//        System.out.println(sw?"Producto Eliminado": "Producto no Eliminado");

//        boolean sw = productoDAO.eliminarPorNombre("HOLA PRODUCTO");
//        System.out.println(sw?"Producto Eliminado": "Producto no Eliminado");

        Producto productoActualizar=new Producto();
        productoActualizar.setId(6);
        productoActualizar.setCodigo("XXXXX10000");
        productoActualizar.setNombre("NOMBRE 10000");
        boolean sw = productoDAO.actualizar(productoActualizar);
        System.out.println(sw?"Producto Actualizado": "Producto no Actualizado");

        ///////
//        Producto nuevoProducto=new Producto();
//        nuevoProducto.setCodigo("XXXXXX32");
//        nuevoProducto.setNombre("NOMBRE 32");
//        boolean sw=productoDAO.insertar(nuevoProducto);
//        System.out.println(sw?"Producto Insertado": "Producto no Insertado");
//        System.out.println("El producto Insertado fue:");
//        System.out.println(nuevoProducto);
        System.out.println("Visualizar Productos después de la inserción");
        productos = productoDAO.listar();
        for(Producto producto: productos){
            System.out.println(producto);
        }
    }
}
