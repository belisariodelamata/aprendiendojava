
import java.util.Scanner;

/**
 *
 * @author BELSOFT
 */
public class PrincipalVectores {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("BIENVENIDO");
        
        System.out.println("Por favor digite el número de elementos a almacenar:");
        int numeroElementos=scanner.nextInt();
        int[] valores=new int[numeroElementos];
        System.out.println("------");
//        for (int i = 0; i < valores.length; i++) {
//            valores[i]=(int)(Math.random()*1000);
//            System.out.println(valores[i]);
//        }

        ///Solicitar al usuario cada uno de los valores
        for (int i = 0; i < valores.length; i++) {
            System.out.println("Digite el valor en la posición "+(i+1)+":");
            valores[i]=scanner.nextInt();
        }
        //Recorriendo todos los valores del vector
        System.out.println("La información almacenada en el vector es:");
        for (int i = 0; i < valores.length; i++) {
            System.out.print(valores[i]+",");
        }
        System.out.println("---");
        System.out.println("El número de elementos es "+numeroElementos);
        /////////////////////////
    }
}
